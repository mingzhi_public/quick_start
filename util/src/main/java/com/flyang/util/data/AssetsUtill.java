package com.flyang.util.data;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AssetsUtill {

//    public static String readAssetsContent(Context context, String fileName) {
//        String content = null;
//
//        try {
//            InputStream is = context.getResources().getAssets().open(fileName);
//            InputStreamReader reader = new InputStreamReader(is);
//            BufferedReader bufferedReader = new BufferedReader(reader);
//            StringBuilder sb = new StringBuilder();
//            String lineTxt = null;
//            while ((lineTxt = bufferedReader.readLine()) != null) {
//
//                sb.append(lineTxt);
//
//            }
//            content = sb.toString();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        return content;
//
//
//    }




    public static String readAssetsContent(Context context, String fileNmae) {
        String result = "";
        try {
            InputStream is = context.getAssets().open(fileNmae);
            int lenght = 0;
            lenght = is.available();
            byte[] buffer = new byte[lenght];
            is.read(buffer);
//            result = new String(buffer, "gbk");
            result = new String(buffer, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
