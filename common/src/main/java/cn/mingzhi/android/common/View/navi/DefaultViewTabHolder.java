package cn.mingzhi.android.common.View.navi;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;

/**
 * 作者 zhangbinglei.
 * 日期 2017/3/21.
 * 描述
 */
public class DefaultViewTabHolder extends IViewTabHolder {
    private float zoomMax = 0.3f;
    private ArgbEvaluator evaluator = new ArgbEvaluator(); // ARGB求值器

    private enum State {
        IDLE, GOING_LEFT, GOING_RIGHT
    }

    boolean mFadeEnabled = true;
    State mState;

    private boolean isSmall(float positionOffset) {
        return Math.abs(positionOffset) < 0.0001;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        currentPosition = position;
        currentPositionOffset = positionOffset;
        if (mState == State.IDLE && positionOffset > 0) {
            oldPosition = pager.getCurrentItem();
            mState = position == oldPosition ? State.GOING_RIGHT : State.GOING_LEFT;
        }
        Log.i("DefaultViewTabHolder", "onPageScrolled: " + oldPosition + "," + position + "," + positionOffset + "," + positionOffsetPixels + "," + pager.getCurrentItem());
        boolean goingRight = position == oldPosition;
        if (mState == State.GOING_RIGHT && !goingRight)
            mState = State.GOING_LEFT;
        else if (mState == State.GOING_LEFT && goingRight)
            mState = State.GOING_RIGHT;
        float effectOffset = isSmall(positionOffset) ? 0 : positionOffset;
        ViewGroup mLeft = (ViewGroup) tabsContainer.getChildAt(position);
        ViewGroup mRight = (ViewGroup) tabsContainer.getChildAt(position + 1);
        if (effectOffset == 0) {
            mState = State.IDLE;//滚动停止
            isTouch = false;
        }
        if (isTouch) {
            animateFadeScale(mLeft, mRight, effectOffset, position);
            colorGradient(mLeft, mRight, effectOffset);
        }
    }

    private void colorGradient(ViewGroup mLeft, ViewGroup mRight, float positionOffset) {
        if (mState != State.IDLE) {
            if (mLeft != null) {
                TextView leftText = (TextView) mLeft.getChildAt(0);
                TextView rightText = (TextView) mLeft.getChildAt(2);
                View line = mLeft.getChildAt(1);
                if (leftText != null) {
                    int evaluate = (Integer) evaluator.evaluate(positionOffset, 0XFFFF0000, 0XFF000000);
                    leftText.setTextColor(evaluate);
                    rightText.setTextColor(evaluate);
                    line.setBackgroundColor(evaluate);
                }
            }
            if (mRight != null) {
                TextView leftText = (TextView) mRight.getChildAt(0);
                TextView rightText = (TextView) mRight.getChildAt(2);
                View line = mRight.getChildAt(1);
                if (leftText != null) {
                    int evaluate = (Integer) evaluator.evaluate(positionOffset, 0XFF000000, 0XFFFF0000);
                    leftText.setTextColor(evaluate);
                    rightText.setTextColor(evaluate);
                    line.setBackgroundColor(evaluate);
                }
            }
        }
    }

    protected void animateFadeScale(View left, View right, float positionOffset, int position) {
        if (mState != State.IDLE) {
            if (left != null) {
//                ViewHelper.setAlpha(tabsContainer.getChildAt(position), positionOffset);
                float mScale = 1 + zoomMax - zoomMax * positionOffset;
                ViewHelper.setPivotX(left, left.getMeasuredWidth() * 0.5f);
                ViewHelper.setPivotY(left, left.getMeasuredHeight() * 0.5f);
                ViewHelper.setScaleX(left, mScale);
                ViewHelper.setScaleY(left, mScale);
            }
            if (right != null) {
//                ViewHelper.setAlpha(tabViews.get(position + 1).get("normal"), 1 - positionOffset);
//                ViewHelper.setAlpha(tabViews.get(position + 1).get("selected"), positionOffset);
                float mScale = 1 + zoomMax * positionOffset;
                ViewHelper.setPivotX(right, right.getMeasuredWidth() * 0.5f);
                ViewHelper.setPivotY(right, right.getMeasuredHeight() * 0.5f);
                ViewHelper.setScaleX(right, mScale);
                ViewHelper.setScaleY(right, mScale);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            mFadeEnabled = true;
        }
    }

    /**
     * 缩放view到指定倍率
     *
     * @param value
     * @param v
     */
    public void scal(float value, View v) {
        ObjectAnimator.ofFloat(v, "scaleX", value).setDuration(300).start();
        ObjectAnimator.ofFloat(v, "scaleY", value).setDuration(300).start();
    }

    @Override
    public void onPageSelected(int position) {
        selectedPosition = position;
        if (!isTouch) {//如果不是滑动，则处理
            View v_old = tabsContainer.getChildAt(oldPosition);
            scal(1f, v_old);
            View v_new = tabsContainer.getChildAt(position);
            scal(1 + zoomMax, v_new);
            setTargetColor();
        }
        oldPosition = selectedPosition;
    }

    @Override
    protected void initFirst() {
        final ViewGroup v_old = (ViewGroup) tabsContainer.getChildAt(0);
        setViewColor(v_old, 1);
        ViewHelper.setScaleX(v_old, zoomMax + 1);
        ViewHelper.setScaleY(v_old, zoomMax + 1);
    }

    /**
     * 设置目标颜色
     */
    private void setTargetColor() {
        final View v_old = tabsContainer.getChildAt(oldPosition);
        final View v_new = tabsContainer.getChildAt(selectedPosition);
        ValueAnimator v1 = ValueAnimator.ofFloat(1f, 0f).setDuration(500);
        v1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = Float.valueOf(animation.getAnimatedValue().toString());
                setViewColor(v_old, value);
            }
        });
        v1.start();
        ValueAnimator v2 = ValueAnimator.ofFloat(0f, 1f).setDuration(500);
        v2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = Float.valueOf(animation.getAnimatedValue().toString());
                setViewColor(v_new, value);
            }
        });
        v2.start();
    }

    private void setViewColor(View v, float value) {
        ViewGroup vi = (ViewGroup) v;
        TextView leftText = (TextView) vi.getChildAt(0);
        TextView rightText = (TextView) vi.getChildAt(2);
        View line = vi.getChildAt(1);
        if (leftText != null) {
            int evaluate = (Integer) evaluator.evaluate(value, 0XFF000000, 0XFFFF0000);
            leftText.setTextColor(evaluate);
            rightText.setTextColor(evaluate);
            line.setBackgroundColor(evaluate);
        }
    }
}
