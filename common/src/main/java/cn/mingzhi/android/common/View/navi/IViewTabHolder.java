package cn.mingzhi.android.common.View.navi;

import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * 作者 zhangbinglei.
 * 日期 2017/3/21.
 * 描述
 */
public abstract class IViewTabHolder implements ViewPager.OnPageChangeListener {
    public ViewGroup tabsContainer;//菜单容器
    public boolean isTouch;

    public void setSearchView(View searchView) {
        this.searchView = searchView;
    }

    public  View searchView;

    public void setViewPager(ViewPager viewPager) {
        this.pager = viewPager;
        pager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isTouch = true;
                }
                return false;
            }
        });
    }

    /**
     * 初始化为第一项选中
     */
    protected abstract void initFirst();

    public ViewPager pager;
    int oldPosition = 0;
    int currentPosition = 0;
    float currentPositionOffset = 0;
    int selectedPosition = 0;

    public void setContainer(ViewGroup view) {
        this.tabsContainer = view;
    }

    public View getSearchView(){


        return searchView;
    }


}
