package cn.mingzhi.android.testmodule;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cn.mingzhi.android.common.View.navi.DefaultViewTabHolder;
import cn.mingzhi.android.common.View.navi.NewPagerSlidingTabStrip;
import cn.mingzhi.android.common.View.navi.PagerSlidingTabStrip;

public class TestPagerSlidingTrapActivity extends AppCompatActivity {
    NewPagerSlidingTabStrip slidingTabStrip;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_pager_sliding_trap);
        slidingTabStrip = (NewPagerSlidingTabStrip) findViewById(R.id.pageslidingtrap);
        DefaultViewTabHolder holder = new DefaultViewTabHolder();
        View searchView = LayoutInflater.from(this).inflate(R.layout.search_view, null, false);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestPagerSlidingTrapActivity.this, "点击了搜索", Toast.LENGTH_SHORT).show();
            }
        });
        holder.setSearchView(searchView);
        slidingTabStrip.setiViewTabHolder(holder);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        slidingTabStrip.setViewPager(viewPager);
    }

    class PagerAdapter extends FragmentPagerAdapter implements NewPagerSlidingTabStrip.ViewTabProvider {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new TestFrgment();
        }

        @Override
        public View getPageView(int position) {
            View tabView = LayoutInflater.from(TestPagerSlidingTrapActivity.this).inflate(R.layout.tab, null, false);
            return tabView;
        }

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return position + "";
        }
    }

    public static class TestFrgment extends Fragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//            return super.onCreateView(inflater, container, savedInstanceState);
            return inflater.inflate(R.layout.frag_test, container, false);
        }
    }
}
