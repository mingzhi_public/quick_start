package cn.mingzhi.android.testmodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestNoPaddingTextView extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_no_padding_text_view);
    }
}
