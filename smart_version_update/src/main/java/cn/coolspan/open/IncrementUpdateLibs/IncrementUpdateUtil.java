package cn.coolspan.open.IncrementUpdateLibs;


public class IncrementUpdateUtil {

    static {
        //静态初始化块加载so文件
        System.loadLibrary("bspatch");
    }


    public native static int bspatch(String oldApkPath,
                                     String newApkPath, String patchPath);

}
