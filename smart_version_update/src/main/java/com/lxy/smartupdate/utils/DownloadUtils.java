package com.lxy.smartupdate.utils;

import android.os.Environment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by lxy on 2016/6/28.
 */
public class DownloadUtils {
    
    public static final String SUFFIX_APK = ".apk";
    public static final String SUFFIX_PATCH = ".patch";

    /**
     * 根据Url获取下载文件的名字
     *
     * @param downloadUrl
     * @return
     */
    public static String getDownloadFileName(String downloadUrl , String downloadVersion) {
        int startNum = downloadUrl.lastIndexOf("/") + 1;
        String name = downloadUrl.substring(startNum, downloadUrl.length());
        if (name.endsWith(SUFFIX_APK)){
            name = name.replace(SUFFIX_APK,"_v"+downloadVersion+SUFFIX_APK);
        }else{
            name = name.replace(SUFFIX_PATCH,"_v"+downloadVersion+SUFFIX_PATCH);
        }
        return name;
    }


    /**
     * 检查SD卡是否挂载
     * @return
     */
    public static boolean isSDCard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果服务器不支持中文路径的情况下需要转换url的编码。
     */
    public static String encodeGB(String string) {
        // 转换中文编码
        String split[] = string.split("/");
        for (int i = 1; i < split.length; i++) {
            try {
                split[i] = URLEncoder.encode(split[i], "GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            split[0] = split[0] + "/" + split[i];
        }
        split[0] = split[0].replaceAll("\\+", "%20");// 处理空格
        return split[0];
    }

}

