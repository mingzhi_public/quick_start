package com.lxy.smartupdate.utils;

import cn.coolspan.open.IncrementUpdateLibs.IncrementUpdateUtil;

/**
 * Created by lxy on 2016/6/27.
 */
public class SmartUpdateUtils {

    /**
     * 
     * @param sourceApkPath
     * @param patchPath
     * @param newApkPath
     * @return
     */
    public static int smartVersionUpdate(String sourceApkPath , String patchPath , String newApkPath){
        return IncrementUpdateUtil.bspatch(sourceApkPath,newApkPath,patchPath);
    }
}
