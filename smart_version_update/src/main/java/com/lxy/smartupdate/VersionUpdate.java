package com.lxy.smartupdate;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.lxy.smartupdate.utils.ApkUtils;
import com.lxy.smartupdate.utils.DownloadUtils;
import com.lxy.smartupdate.utils.SmartUpdateUtils;

import java.io.File;

/**
 * Created by lxy on 2016/6/28.
 */
public class VersionUpdate {


    /**
     * 增量更新-合成成功
     */
    public static final int WHAT_SUCCESS = 1;

    /**
     * 增量更新-合成失败
     */
    public static final int WHAT_FAIL_PATCH = -3;

    /**
     * 增量更新-获取源apk失败
     */
    public static final int WHAT_FAIL_GET_SOURCE = -4;
    
    

    private String mDownloadFileName;
    
    private String mDownloadUrl;
    
    private DownloadManager mDownloadManager;

    private DownloadCompleteReceiver mDownloadCompleteReceiver;

    private int mLocalVersionCode;

    private String mSourceApkPath;
    
    private String mAppName;
    
    private String mMergeApkPath;
    
    private long mDownloadId = 0;
    
    private SharedPreferences mDefaultSP;

    // ---- from build ----

    private String mDownloadTitle;

    private String mPackageName;

    private Context mContext;

    private String mApkDownloadUrl;
    
    private String mPatchDownloadUrl;

    private String mServerVersionName;

    private int mServerVersionCode;

    private String mStoragePath;

    private boolean isNotificationShow;

    private boolean isSmartUpdate;

    private boolean isToastShow;


    private VersionUpdate(Build build) {

        _checkThreadOnUI();
        _initVariable(build);
        _initLocalPath();
        
        getDownloadDetails();
    }

    public void startUpdate() {
        if (checkActivityDestroy()) {
            return;
        }
        startDownload();
    }


    public void cancel() {
        if (!checkActivityDestroy()) {
            if (mDownloadCompleteReceiver!=null){
                mContext.unregisterReceiver(mDownloadCompleteReceiver);
//                mContext = null;
                mDownloadCompleteReceiver = null;
            }
        }
        
        if (mDownloadManager!=null){
            
            if (mDownloadId!=0){
                mDownloadManager.remove(mDownloadId);
                int downloadStatus = getDownloadStatus(mDownloadId);
                if (downloadStatus != DownloadManager.STATUS_SUCCESSFUL){
                    if (getDownloadFilePath().exists()){
                        getDownloadFilePath().delete();
                    }
                }
            }
            
        }

    }

    private void getDownloadDetails() {

        mDownloadUrl = useSmartUpdate() ? mPatchDownloadUrl : mApkDownloadUrl;

        mDownloadFileName = DownloadUtils.getDownloadFileName(mDownloadUrl, String.valueOf(mServerVersionCode));
        
        if (useSmartUpdate()) {
            
            if (mDownloadFileName.endsWith(".apk")) {
                throw new RuntimeException("smart update can't use .apk download url");
            }

        } else {
            
            if (mDownloadFileName.endsWith(".patch")) {
                throw new RuntimeException("apk version update can't use .patch download url");
            }
        }
        
        
    }

    private void startDownload() {

        // 检查文件是否存在
        File pathExists = getDownloadFilePath();

        // 不存在 那么为SD为挂载
        if (pathExists == null) {
            showToast("SD卡未挂载");
            return;
        }
        
        if (pathExists.exists()) { // 1.为patch 2.为apk
            
            long localDownloadId = mDefaultSP.getLong(mDownloadUrl,0);
            int localStatus = getDownloadStatus(localDownloadId);
            switch (localStatus){
                case DownloadManager.STATUS_SUCCESSFUL:
                    if (useSmartUpdate()) {
                        new PatchMergeTask().execute();
                        return;
                    } else {
                        ApkUtils.installApk(mContext, pathExists.getAbsolutePath());
                        return;
                    }
                case DownloadManager.STATUS_RUNNING:
                    showToast("正在下载");
                    return;
            }
            
            
        }

        showToast("正在下载");

        Uri resource = Uri.parse(DownloadUtils.encodeGB(mDownloadUrl));

        // 初始化 下载请求
        Request request = new Request(resource);

        // 设置下载请求网络及其他
        request.setAllowedNetworkTypes(Request.NETWORK_MOBILE | Request.NETWORK_WIFI);
        request.setAllowedOverRoaming(false);

        // 设置文件类型
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(mDownloadUrl));
        request.setMimeType(mimeString);

        // 设置在通知栏是否显示
        if (isNotificationShow) {
            // 点击后才会消失
            request.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            // 是否显示UI在系统中
            request.setVisibleInDownloadsUi(true);
            // 设置标题
            request.setTitle(mDownloadTitle);
        } else {
            request.setNotificationVisibility(Request.VISIBILITY_HIDDEN);
            request.setVisibleInDownloadsUi(false);
        }

        // 设置下载位置与下载名称
        String downloadPath = mStoragePath.replace(Environment.getExternalStorageDirectory() + "", "");
        request.setDestinationInExternalPublicDir(downloadPath, mDownloadFileName);


        // 加入下载队列开始下载并保存下载ID
        mDownloadId = mDownloadManager.enqueue(request);
        mDefaultSP.edit().putLong(mDownloadUrl,mDownloadId).commit();

        // 注册广播监听下载状态
        mDownloadCompleteReceiver = new DownloadCompleteReceiver();
        mContext.registerReceiver(mDownloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    private void downloadComplete(long downloadId) {
        Cursor c = getDownloadCursor(downloadId);
        if (c.moveToFirst()) {
            int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
            switch (status) {
                case DownloadManager.STATUS_SUCCESSFUL:
                    if (useSmartUpdate()) {
                        
                        new PatchMergeTask().execute();
                    } else {

                        ApkUtils.installApk(mContext, c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME)));
                        showToast("下载完成");
                    }

                    break;
                case DownloadManager.STATUS_FAILED:
                    // 清除已下载的内容，重新下载
                    mDownloadManager.remove(downloadId);
                    if (getDownloadFilePath().exists()){
                        getDownloadFilePath().delete();
                    }
                    startUpdate();
                    
                    break;
            }
        }
    }

    private int getDownloadStatus(long downloadId){

        int status = -1;
        Cursor c = getDownloadCursor(downloadId);

        if (c.moveToFirst()) {
            status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
        }

        return status;
    }
    
    private Cursor getDownloadCursor(long downloadId){
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);
        return mDownloadManager.query(query);
    }
    
    


    private void _initVariable(Build build) {
        
        this.mContext = build.mContext;
        this.mPackageName = mContext.getPackageName();
        this.mDownloadTitle = build.mDownloadTitle;
        this.mLocalVersionCode = ApkUtils.getVersionCode(mContext, mPackageName);
        this.mServerVersionCode = build.mServerVersionCode;
        this.mServerVersionName = build.mServerVersionName;
        this.mApkDownloadUrl = build.mApkDownloadUrl;
        this.mPatchDownloadUrl = build.mPatchDownloadUrl;
        this.mStoragePath = build.mStoragePath;
        this.mSourceApkPath = ApkUtils.getSourceApkPath(mContext, mPackageName);
        this.isNotificationShow = build.isNotificationShow;
        this.isSmartUpdate = build.isSmartUpdate;
        this.isToastShow = build.isToastShow;
        this.mDownloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        this.mAppName = ApkUtils.getAppName(mContext,mPackageName);
        this.mMergeApkPath = mStoragePath+mAppName+"_v"+mServerVersionCode+DownloadUtils.SUFFIX_APK;
        this.mDefaultSP = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    private void _initLocalPath() {
        File file = new File(mStoragePath);
        if (!file.exists()) {
            file.mkdirs();
        } else {
            // 删除当前版本的apk安装包
            String nowApkName = "_v" + String.valueOf(mLocalVersionCode);
            File[] files = file.listFiles();
            for (File f : files) {
                String fileName = f.getName();
                if (fileName.contains(nowApkName) && fileName.endsWith(".apk")) {
                    f.delete();
                }

            }
        }
    }

    private void _checkThreadOnUI() {

        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new RuntimeException("version update must in UI thread");
        }
    }

    private boolean useSmartUpdate() {
        return isSmartUpdate;
    }
    
    private void setSmartUpdate(boolean is){
        isSmartUpdate = is;   
    }

    private boolean checkActivityDestroy() {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return ((Activity) mContext).isDestroyed();
        }
        return mContext == null;
    }

    private File getDownloadFilePath() {
        return new File(mStoragePath, mDownloadFileName);
    }

    private void showToast(String msg) {
        if (!isToastShow) {
            return;
        }
        if (mContext != null) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        }
    }

    // down load complete will be call this receiver
    class DownloadCompleteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            downloadComplete(downloadId);
        }
    }

    class PatchMergeTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showToast("增量更新开始");
        }

        @Override
        protected Integer doInBackground(String... params) {


            if (new File(mMergeApkPath).exists()) {
                return WHAT_SUCCESS;
            }

            if (!TextUtils.isEmpty(mSourceApkPath)) {

                // patch merge 
                int patchResult = SmartUpdateUtils.smartVersionUpdate(mSourceApkPath, getDownloadFilePath().getAbsolutePath(), mMergeApkPath);

                if (patchResult == 0) { // 0 is successful
                
                    return WHAT_SUCCESS;
                    
                } else {
                    
                    return WHAT_FAIL_PATCH;
                }

            } else {
            
                return WHAT_FAIL_GET_SOURCE;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            
            // delete .patch file
            File file = getDownloadFilePath();
            if (file.exists()) {
                file.delete();
            }

            switch (result) {
                
                case WHAT_SUCCESS: {
                    ApkUtils.installApk(mContext, mMergeApkPath);
                    showToast("合并完成");
                    break;
                }

                case WHAT_FAIL_PATCH: {
                    String text = "文件下载异常，开始整包更新";
                    showToast(text);
                    if (!TextUtils.isEmpty(mApkDownloadUrl)){
                        
                        setSmartUpdate(false);
                        startUpdate();
                    }
                    
                    break;
                }
                
                case WHAT_FAIL_GET_SOURCE: {
                    String text = "无法获取客户端的源apk文件，开始整包更新";
                    showToast(text);
                    
                    if (!TextUtils.isEmpty(mApkDownloadUrl)){

                        setSmartUpdate(false);
                        startUpdate();
                    }
                    break;
                }
            }
        }

    }
        public static class Build {

            private String mDownloadTitle ;

            private Context mContext;

            private int mServerVersionCode;

            private String mServerVersionName;
            
            private String mApkDownloadUrl;
            
            private String mPatchDownloadUrl;

            private String mStoragePath;

            private boolean isNotificationShow = true;

            private boolean isSmartUpdate = false;

            private boolean isToastShow = true;

            private Build() {

            }

            public Build(Context context) {
                this.mContext = context;
                this.mDownloadTitle = ApkUtils.getAppName(mContext,mContext.getPackageName());
            }

            public Build setDownloadTitle(String downloadTitle) {
                this.mDownloadTitle = downloadTitle;
                return this;
            }

            public Build setServerVersionCode(int serverVersionCode) {
                this.mServerVersionCode = serverVersionCode;
                return this;
            }

            public Build setServerVersionName(String serverVersionName) {
                this.mServerVersionName = serverVersionName;
                return this;
            }

            public Build setApkDownloadUrl(String apkDownloadUrl){
                this.mApkDownloadUrl = apkDownloadUrl;
                return this;
            }
            
            public Build setPatchDownloadUrl(String patchDownloadUrl){
                this.mPatchDownloadUrl = patchDownloadUrl;
                return this;
            }
            
            public Build setStoragePath(String storagePath) {
                this.mStoragePath = storagePath;
                return this;
            }

            public Build setNotificationShow(boolean isShow) {
                this.isNotificationShow = isShow;
                return this;
            }

            public Build setSmartUpdate(boolean isSmartUpdate) {
                this.isSmartUpdate = isSmartUpdate;
                return this;
            }

            public Build setToastShow(boolean isToastShow) {
                this.isToastShow = isToastShow;
                return this;
            }

            public VersionUpdate build() {
                return new VersionUpdate(this);
            }

        }
    }
