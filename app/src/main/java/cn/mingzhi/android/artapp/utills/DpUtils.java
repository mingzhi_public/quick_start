package cn.mingzhi.android.artapp.utills;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;


//工具类
public class DpUtils {

    private static  String LOG_TAG = "云推送添加分组";

    /**
	 * This method converts device specific pixels to device independent pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param px          
	 *            px（像素）转换成db
	 * @param

	 * @param
	 *
	 * @return A float value to represent db equivalent to px value
	 * @return 注意db和px的区别
	 */
	public static float convertPixelsToDp(Context ctx, float px) {
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;

	}

	public static int convertDpToPixelInt(Context context, float dp) {

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int px = (int) (dp * (metrics.densityDpi / 160f));
		return px;
	}
	
	public static float convertDpToPixel(Context context, float dp) {

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float px = (float) (dp * (metrics.densityDpi / 160f));
		return px;
	}

	public static float dp2Px(Context context,float dp) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,context. getResources().getDisplayMetrics());
	}





}
