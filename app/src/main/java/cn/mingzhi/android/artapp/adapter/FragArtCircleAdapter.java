package cn.mingzhi.android.artapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.bean.FragMessageBean;
import cn.mingzhi.android.artapp.listener.MyMutiOnitemClickListener;

/**
 * Created by lugang on 2016/1/28.
 */
public class FragArtCircleAdapter extends RecyclerView.Adapter<FragArtCircleAdapter.FragMessageHolder> {
    private final SparseBooleanArray mCollapsedStatus;
    private Context context;

    private List<FragMessageBean> list;


    private MyMutiOnitemClickListener myMutiOnitemClickListener;

    public FragArtCircleAdapter(Context ctx, MyMutiOnitemClickListener mutiOnitemClickListener) {
        this.context = ctx;
        this.list = new ArrayList<>();
        mCollapsedStatus = new SparseBooleanArray();

        this.myMutiOnitemClickListener=mutiOnitemClickListener;
    }

    public void setList(List<FragMessageBean> list) {
        this.list = list;
        this.notifyDataSetChanged();
    }

    @Override
    public FragMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.frag_message_item, parent, false);

        return new FragMessageHolder(view);
    }

    @Override
    public void onBindViewHolder(FragMessageHolder holder, int position) {

//        holder.expand_text_view.setText(sampleStrings[position], mCollapsedStatus, position);


        holder.update(position);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class FragMessageHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView titleTv,timeTv,contentTv,noReadCountTv,exampleTv;



        View itemView;

        public FragMessageHolder(View itemView) {
            super(itemView);

            this.itemView=itemView;

            icon=(ImageView)itemView.findViewById(R.id.frag_message_item_icon);
            titleTv=(TextView) itemView.findViewById(R.id.frag_message_item_title);
            timeTv=(TextView) itemView.findViewById(R.id.frag_message_item_time);
            contentTv=(TextView) itemView.findViewById(R.id.expandable_text);
            noReadCountTv=(TextView) itemView.findViewById(R.id.frag_message_item_noReadCount);
            exampleTv=(TextView) itemView.findViewById(R.id.frag_message_item_example);







        }

        public void update(final int position) {


            FragMessageBean itemBean = list.get(position);


        }
    }


}
