package cn.mingzhi.android.artapp.base;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.mingzhi.android.artapp.application.NewsApplication;
import cn.mingzhi.android.artapp.bean.SettingBean;
import cn.mingzhi.android.artapp.bean.UserBean;
import cn.mingzhi.android.artapp.listener.MyReceiveDataListener;
import cn.mingzhi.android.artapp.network.NetWorkUtill;
import cn.mingzhi.android.artapp.utills.SharePreferenceUtill;
import cn.mingzhi.android.artapp.widged.CustomProgressDialog;
import cn.mingzhi.android.artapp.widged.ShareDialog;


public abstract class BaseFragment extends Fragment implements
        MyReceiveDataListener,
        View.OnClickListener


{


    /**
     * 控制频频播放的service
     */


    protected String DEVICE_IMEI;

    public String LOG_TAG = getClass().getName();

    protected static String TAG = BaseFragment.class.getSimpleName();
    public NewsApplication mApp;


    public String OK = "0";
    protected boolean isVisible;

    private ImageView iv_speech_volume;
    private TextView tv_speech_title;
    public NetWorkUtill netWorkUtill;
    public int limit = 20;
    private View speech_dilog_bg;
    private static List<View> imageViewList = new ArrayList<View>();
    /**
     * when activity is recycled by system, isFirstTimeStartFlag will be reset to default true,
     * when activity is recreated because a configuration change for example screen rotate, isFirstTimeStartFlag will stay false
     */
    private boolean isFirstTimeStartFlag = true;

    protected final static int FIRST_TIME_START = 0; //when activity is first time start
    protected final static int SCREEN_ROTATE = 1;    //when activity is destroyed and recreated because a configuration change, see setRetainInstance(boolean retain)
    protected final static int ACTIVITY_DESTROY_AND_CREATE = 2;  //when activity is destroyed because memory is too low, recycled by android system


    private boolean isAddObserver = false;
    public int position = 1;//当前Fragment索引

    protected int getCurrentState(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            isFirstTimeStartFlag = false;
            return ACTIVITY_DESTROY_AND_CREATE;
        }


        if (!isFirstTimeStartFlag) {
            return SCREEN_ROTATE;
        }

        isFirstTimeStartFlag = false;
        return FIRST_TIME_START;
    }

    public ShareDialog shareDialog;




    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }




    /**
     * 语音识别音量变化
     */
    boolean isFirst;
    int width = 0;


    /**
     * 可见
     */
    protected void onVisible() {
        lazyLoad();
    }


    /**
     * 不可见
     */
    protected void onInvisible() {


    }

    /**
     * 判断是不是绑定服务
     *
     * @return
     */
    public boolean isBind() {


        if (getActivity() instanceof BaseActivity) {


            BaseActivity baseActivity = (BaseActivity) getActivity();

            return baseActivity.mBound;
        }

        return false;
    }


    /**
     * 延迟加载
     * 子类必须重写此方法
     */
    protected abstract void lazyLoad();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(false);

        mApp = NewsApplication.getInstance();



        netWorkUtill = new NetWorkUtill();
        shareDialog = new ShareDialog(getActivity());

        System.out.println("test position=" + position);


    }

    public View rootView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = initView(inflater);


        return rootView;
    }


    public abstract View initView(LayoutInflater inflater);

    public abstract void loadData();

    public View getRootView() {
        return rootView;
    }

    @Override
    public void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
    }


    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
        if (isAddObserver) {

            isAddObserver = true;
        }

    }

    @Override
    public void onDestroyView() {
        Log.v(TAG, "onDestroyView");
        super.onDestroyView();
    }

    /**
     * 查找View
     *
     * @param id   控件的id
     * @param <VT> View类型
     * @return
     */
    protected <VT extends View> VT getViewById(@IdRes int id) {
        return (VT) rootView.findViewById(id);
    }


    @Override
    public void onReceive(int action, String code, String msg, Object data) {

    }

    @Override
    public void onFail(int action, Object data) {

        showToast("无网络链接");

    }

    private CustomProgressDialog progressDialog;

    public SettingBean setSetting() {

        return SharePreferenceUtill.getInstance(getActivity()).getSetting();

    }

    public void saveSetting(SettingBean settingBean) {

        SharePreferenceUtill.getInstance(getActivity()).saveSettingBean(settingBean);
    }

    public UserBean getUserBean() {

//        return SharePreferenceUtill.getInstance(this).getUserbean();

//        private String nickName;
//        private String userId;
//        private String mobile;
//        private String avatar;



        return null;


    }

    public SettingBean getSetting() {
        SettingBean settingBean = SharePreferenceUtill.getInstance(getActivity()).getSetting();
        if (settingBean == null) {
            settingBean = new SettingBean();
        }
        return settingBean;

    }

    public void saveUserBean(UserBean userBean) {

        SharePreferenceUtill.getInstance(getActivity()).saveUserBean(userBean);
    }

    public void startProgressDialog() {


        stopProgressDialog();

        progressDialog = CustomProgressDialog.createDialog(getActivity());


        progressDialog.show();


    }

    /**
     * 停止运行自定义的dialog
     */
    public void stopProgressDialog() {


        if (progressDialog != null) {


            progressDialog.dismiss();
//                    progressDialog.dismissAllowingStateLoss();
            progressDialog = null;
        } else {


        }
    }


    public void showToast(String text) {

        Toast toast = NewsApplication.getInstance().getToast();

        toast.setText(text);


        toast.show();
    }



    @Override
    public void onClick(View v) {

    }




}
