package cn.mingzhi.android.artapp.listener;

/**
 * Created by zhangbinglei on 16/4/27.
 */
public interface ReceiveListener {

    void onReceive(Object data);


    void onFail(Object data);
}
