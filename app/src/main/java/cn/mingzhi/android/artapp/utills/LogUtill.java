package cn.mingzhi.android.artapp.utills;

import android.util.Log;

import cn.mingzhi.android.artapp.config.Config;


/**
 * Created by zhangbinglei on 16/4/26.
 */
public class LogUtill {


    private static boolean LOG_TAG = true;

    static {

        LOG_TAG = Config.DEBUG;
    }

    public static void log_i(String className, String tag, String text) {
        if (LOG_TAG) {
            Log.i(className, tag + "----->>" + text);
        }
    }

    public static void print(String className, String tag, String text) {
        if (LOG_TAG) {
            Log.i(className, tag + "------>>" + text);
        }
    }


    public static void json(String className, String tag, String text) {
        if (LOG_TAG) {
//            Log.i(className, tag + "------>>" + text);


        }
    }


    public static void print(String className, String text) {
        print(className, "", text);

    }
}
