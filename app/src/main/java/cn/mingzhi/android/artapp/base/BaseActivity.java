package cn.mingzhi.android.artapp.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.lxy.smartupdate.VersionUpdate;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;
import java.util.List;


import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.activity.LoginAct;
import cn.mingzhi.android.artapp.application.NewsApplication;
import cn.mingzhi.android.artapp.bean.CheckVersionBean;
import cn.mingzhi.android.artapp.bean.SettingBean;
import cn.mingzhi.android.artapp.bean.UserBean;
import cn.mingzhi.android.artapp.constants.Constants;
import cn.mingzhi.android.artapp.interfaces.ShareClickListener;
import cn.mingzhi.android.artapp.listener.MyReceiveDataListener;
import cn.mingzhi.android.artapp.network.NetWorkUtill;
import cn.mingzhi.android.artapp.utills.SharePreferenceUtill;
import cn.mingzhi.android.artapp.utills.StringUtill;
import cn.mingzhi.android.artapp.utills.Utils;
import cn.mingzhi.android.artapp.widged.CustomProgressDialog;
import cn.mingzhi.android.artapp.widged.ShareDialog;



/**
 * Created by lugang on 2016/1/20.
 */
public class BaseActivity extends AppCompatActivity implements View.OnClickListener,MyReceiveDataListener{

    public int statAction = 0;

    protected String DEVICE_IMEI;



    public boolean mBound;


    public static Context context;

    private ShareDialog shareDialog;

    private WebSettings settings;
    private Dialog loadingDialog;
    public NetWorkUtill netWorkUtill;
    public String LOG_TAG = "BaseActivity";
    /**
     * 控制WebView字体
     */
    public static final int TEXTSIZE_SMALLEST = 0;
    public static final int TEXTSIZE_SMALLER = 25;
    public static final int TEXTSIZE_NORMAL = 50;
    public static final int TEXTSIZE_LARGER = 75;
    public static final int TEXTSIZE_LARGEST = 100;
    private CustomProgressDialog progressDialog;
    public int limit = 20;


    public String Ok = "0";
    /**
     * 用来控制WebView中字体的大小
     */





    private SharedPreferences sharedPreferences;
    private static final String DL_ID = "downloadIdsdfs";
    private DownloadManager downloadManager;
    private Dialog updateDialog;



    private VersionUpdate mVersionUpdate;

    private String mDownloadPath;


    /*private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        netWorkUtill = new NetWorkUtill();


        sharedPreferences = ((NewsApplication) getApplication()).sharedPreferences;

        LOG_TAG = getClass().getName();
        Constants.print(LOG_TAG, "进入的页面", LOG_TAG);


    }


    public void setLayoutManager(RecyclerView recyclerView) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        if (mVersionUpdate!=null){
            mVersionUpdate.cancel();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    /**
     * 查找View
     *
     * @param id   控件的id
     * @param <VT> View类型
     * @return
     */
    protected <VT extends View> VT getViewById(@IdRes int id) {
        return (VT) findViewById(id);
    }



    public void initArgs() {

    }

    public void initView() {

    }

    /**
     *统计分享
     */

    public void statShare(String id,SHARE_MEDIA platform){
        String pf = "";
        if (platform == SHARE_MEDIA.WEIXIN){
            pf = "1";
        }else if (platform == SHARE_MEDIA.WEIXIN_CIRCLE){
            pf = "2";
        }else if (platform == SHARE_MEDIA.YIXIN){
            pf = "3";
        }else if (platform == SHARE_MEDIA.YIXIN_CIRCLE){
            pf = "4";
        }else if (platform == SHARE_MEDIA.QQ){
            pf = "5";
        }else if (platform == SHARE_MEDIA.QZONE){
            pf = "6";
        }else if (platform == SHARE_MEDIA.SINA){
            pf = "7";
        }


    }


    public void initWebViewArgs(WebView webView) {
        webView.setWebChromeClient(new MyWebChromeClient());

        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setSupportZoom(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
    }


    /**
     * 为了捕捉js的 alert在webview不能够直接的显示 需要一个webchromeclient容器
     *
     * @author Administrator
     */
    public class MyWebChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message,
                                 final JsResult result) {
            new AlertDialog.Builder(BaseActivity.this)
                    .setTitle("提醒")
                    .setMessage(message)
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            result.confirm();
                        }
                    })
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            result.cancel();
                        }
                    }).create().show();

            return true;
        }
    }



    /**
     * 展开分享的dialog
     *
     * @param shareContent 分享内容
     * @param TargetUrl    分享跳转链接
     * @param imageUrl     分享图片地址
     * @param title        分享标题
     */
    public void showShareDialog(final String articleId,final String shareContent, final String TargetUrl, String imageUrl, final String title) {
        String[] mPermissionList = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS};
        ActivityCompat.requestPermissions(BaseActivity.this, mPermissionList, 100);
        if (shareDialog == null) {
            shareDialog = new ShareDialog(this);
        }
        UMImage image = null;
        if (!TextUtils.isEmpty(imageUrl)) {
            image = new UMImage(this, imageUrl);
        }else {
            image = new UMImage(BaseActivity.this,
                    BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        }
        final UMImage finalImage = image;
        shareDialog.setOnShareListener(new ShareClickListener() {
            @Override
            public void onClick(SHARE_MEDIA platform) {
                shareDialog.dismiss();
                    new ShareAction(BaseActivity.this)
                            .setPlatform(platform)
                            .setCallback(umShareListener)
                            .withText(shareContent)
                            .withTargetUrl(TargetUrl)
                            .withMedia(finalImage)
                            .withTitle(title)
                            .share();

                statShare(articleId,platform);

            }

            @Override
            public void onCopy() {
                shareDialog.dismiss();
                ClipboardManager cmb = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                cmb.setText(TargetUrl);
                showToast("已复制链接到剪贴板");
            }
        });
        shareDialog.show();
    }

    /**
     * 分享微信或者微博
     *
     * @param shareContent 分享内容
     * @param TargetUrl    分享跳转链接
     * @param imageUrl     分享图片地址
     * @param title        分享标题
     * @param type         1微信好友 2微信朋友圈 3新浪文博
     */
    public void shareWechatOrSina(final String articleId,int type, final String shareContent, final String TargetUrl, String imageUrl, final String title) {
        String[] mPermissionList = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS};
        ActivityCompat.requestPermissions(BaseActivity.this, mPermissionList, 100);
        SHARE_MEDIA platform = null;
        if (type == 1) {
            platform = SHARE_MEDIA.WEIXIN;
        } else if (type == 2) {
            platform = SHARE_MEDIA.WEIXIN_CIRCLE;
        } else {
            platform = SHARE_MEDIA.SINA;
        }
        if(shareDialog==null){
            shareDialog = new ShareDialog(this);
        }
        statShare(articleId,platform);
        UMImage image = null;
        if (!StringUtill.isEmpty(imageUrl)) {
            image = new UMImage(this, imageUrl);
        }else {
            image = new UMImage(BaseActivity.this,
                    BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        }
        final UMImage finalImage = image;
        new ShareAction(BaseActivity.this)
                .setPlatform(platform)
                .setCallback(umShareListener)
                .withText(shareContent)
                .withTargetUrl(TargetUrl)
                .withMedia(finalImage)
                .withTitle(title)
                .share();
    }

    UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA share_media) {
            showToast("分享成功");
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            showToast("分享失败");
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            showToast("取消分享");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }



    public SettingBean getSetting() {
        SettingBean settingBean = SharePreferenceUtill.getInstance(this).getSetting();

        if (settingBean == null) {
            settingBean = new SettingBean();
        }
        return settingBean;

    }

    public void saveSetting(SettingBean settingBean) {

        SharePreferenceUtill.getInstance(this).saveSettingBean(settingBean);
    }

    public UserBean getUserBean() {

//        return SharePreferenceUtill.getInstance(this).getUserbean();

//        private String nickName;
//        private String userId;
//        private String mobile;
//        private String avatar;


        return null;


    }

    public void saveUserBean(UserBean userBean) {

        SharePreferenceUtill.getInstance(this).saveUserBean(userBean);
    }





    public void initTitle(String text) {


        initTitle(text, true, "");

    }
    public void setAppFirstState(){
        sharedPreferences.edit().putString("is_first","1").commit();
    }
    public String getAppFirstState(){
        return sharedPreferences.getString("is_first","0");
    }
    public void initTitle(String text, String right) {


        initTitle(text, true, right);

    }

    public void initTitle(String text, boolean isBack, String right) {


        if (isBack) {

            findViewById(R.id.back).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.back).setVisibility(View.GONE);
        }


        TextView rightTv = (TextView) findViewById(R.id.right);


        if (!StringUtill.isEmpty(right)) {
            if (rightTv != null) {
                rightTv.setText(right);
                rightTv.setVisibility(View.VISIBLE);
                rightTv.setOnClickListener(this);

            }

        } else {
            if (rightTv != null) {
                rightTv.setVisibility(View.GONE);


            }

        }

        findViewById(R.id.back).setOnClickListener(this);

        TextView titleTv = (TextView) findViewById(R.id.title);

        titleTv.setText(text);
    }


    public void startProgressDialog() {


        stopProgressDialog();

        progressDialog = CustomProgressDialog.createDialog(this);


        progressDialog.show();


    }

    /**
     * 停止运行自定义的dialog
     */
    public void stopProgressDialog() {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {


                    progressDialog.dismiss();
//                    progressDialog.dismissAllowingStateLoss();
                    progressDialog = null;
                } else {


                }
            }
        });

    }

    public void showToast(String text) {

        Toast toast = NewsApplication.getInstance().getToast();

        toast.setText(text);


        toast.show();
    }

    @Override
    public void onReceive(int action, String code, String msg, Object data) {

    }

    @Override
    public void onFail(int action, Object data) {

        showToast(data.toString());
        dismissLoadingProgressDialog();

    }


//
//    @Override
//    public void startActivityForResult(Intent intent, int requestCode) {
//        super.startActivityForResult(intent, requestCode);
//        overridePendingTransition(R.anim.base_activity_right2left_on, R.anim.base_activity_right2left_off);
//    }
//
//    @Override
//    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
//        super.startActivityForResult(intent, requestCode, options);
//        overridePendingTransition(R.anim.base_activity_right2left_on, R.anim.base_activity_right2left_off);
//    }
//
//    @Override
//    public void startActivity(Intent intent) {
//        super.startActivity(intent);
//        overridePendingTransition(R.anim.base_activity_right2left_on, R.anim.base_activity_right2left_off);
//    }
//
//    @Override
//    public void startActivity(Intent intent, Bundle options) {
//        super.startActivity(intent, options);
//        overridePendingTransition(R.anim.base_activity_right2left_on, R.anim.base_activity_right2left_off);
//    }

//    @Override
//    public void finish() {
//        super.finish();
//        overridePendingTransition(R.anim.base_activity_left2right_on, R.anim.base_activity_left2right_off);
//    }
//




    /**
     * 隐藏加载中dialog
     */
    public void dismissLoadingProgressDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }


    /**
     * 语音识别音量变化
     */
    boolean isFirst;
    int width = 0;




    public Object getVersion(int type) {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            if (type == 0) {
                String version = info.versionName;
                return version;
            } else {
                int version = info.versionCode;
                return version;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserBean checkLogin() {

        return SharePreferenceUtill.getInstance(this).getUserbean();

    }

    /**
     * 展示版本更新dialog
     * @param checkVersionBean 版本更新实体bean
     */
    public void showUpdateDialog(CheckVersionBean checkVersionBean) {
        if (updateDialog == null) {
            createUpdateDialog(checkVersionBean);
        } else {
            updateDialog.show();
        }
    }
    
    /**
     * 创建版本更新dialog
     *
     * @param checkVersionBean 版本更新实体bean
     */
    private void createUpdateDialog(final CheckVersionBean checkVersionBean) {

        boolean isCompulsionUpdate = "2".equals(checkVersionBean.getIs_needs_update()); // 是否为强制更新
        if (isCompulsionUpdate){
            startVersionUpdate(checkVersionBean);
            return;
        }
        
        updateDialog = new Dialog(this, R.style.dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_update_version, null);
        Window window = updateDialog.getWindow();
        window.setContentView(view);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.height = Utils.dip2px(this, 280);
        wl.width = Utils.getDisplaySize(this).widthPixels - Utils.dip2px(this, 60);
        window.setAttributes(wl);

        Button btn_update = (Button) view.findViewById(R.id.btn_update);
        Button btn_close = (Button) view.findViewById(R.id.btn_close);
        TextView tv_content = (TextView) view.findViewById(R.id.tv_content);
        tv_content.setText(checkVersionBean.getMessage());

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissUpdateDialog();
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissUpdateDialog();
                startVersionUpdate(checkVersionBean);
            }
        });
        updateDialog.show();
    }

    /**
     * 隐藏版本更新dialog
     */
    private void dismissUpdateDialog() {
        if (updateDialog != null && updateDialog.isShowing()) {
            updateDialog.dismiss();
        }
    }
    
    private void startVersionUpdate(CheckVersionBean versionBean){

        _initVersionUpdateVariable(versionBean);
        _startUpdate();
    }

    private void _initVersionUpdateVariable(CheckVersionBean versionBean) {

        String downloadTitle = getResources().getString(R.string.app_name) + "新版本";
        
        int versionCode = Integer.parseInt(versionBean.getVersion_code());
        
        boolean useSmartUpdate = "1".equals(versionBean.getIs_patch());
        
        // 是否为强制更新 true-需要强制更新 false-不需要强制更新
        boolean isCompulsionUpdate = "2".equals(versionBean.getIs_needs_update()); // 是否为强制更新
        
        mDownloadPath = Environment.getExternalStorageDirectory()
                + File.separator
                + getResources().getString(R.string.app_name)
                + File.separator
                + "Versions"
                + File.separator;

        mVersionUpdate = new VersionUpdate.Build(this)
                .setServerVersionCode(versionCode)
                .setServerVersionName(versionBean.getVersion_name())
                .setSmartUpdate(useSmartUpdate)
                .setToastShow(!isCompulsionUpdate)
                .setNotificationShow(!isCompulsionUpdate)
                .setDownloadTitle(downloadTitle)
                .setStoragePath(mDownloadPath)
                .setApkDownloadUrl(versionBean.getDown_load_url())
                .setPatchDownloadUrl(versionBean.getDown_patch_url())
                .build();
    }
    
    private void _startUpdate(){
        if (mVersionUpdate!=null){
            mVersionUpdate.startUpdate();
        }
    }

    @Override
    public void onClick(View view) {

    }
}
