package cn.mingzhi.android.artapp.network;




import org.xutils.common.Callback;
import org.xutils.ex.HttpException;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.net.UnknownHostException;

public class BaseNetworkUtill {

    public static final String OK = "0";
    public static final String NO_TADA = "1";
    public static final String NO_DATA = "1002";
    public static final String JSON_ERRO = "json__parse_erro";//数据解析异常

    public static final String JSON_ERRO_MSG = "数据解析异常";

    private String LOG_TAG = "BaseNetworkUtill";

    public void post(final RequestParams params,
                     final OnReceiveCallback requestCallBack, final boolean catchAble) {

        x.http().post(params, new Callback.CommonCallback<String>() {

            @Override
            public void onSuccess(String result) {

                if (requestCallBack != null) {
                    requestCallBack.onSuccess(result);
                    if (catchAble) {

                    }

                }


            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {


                if (requestCallBack != null) {


                    if (ex instanceof HttpException) {

                        HttpException httpException = (HttpException) ex;

                        requestCallBack.onErro("请求失败，错误码：" + httpException.getCode());
//                        Constants.print(LOG_TAG, "错误类型_httpException_错误码", httpException.getCode() + "--" + httpException.getMessage());


                    } else if (ex instanceof UnknownHostException) {

                        requestCallBack.onErro("网络无连接!");


                    } else {


                        requestCallBack.onErro(ex.getMessage());
                    }


                }


            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });


    }

    /**
     * 默认没网的时候也不走缓存
     * @param params
     * @param requestCallBack
     */
    public void post(final RequestParams params,
                     final OnReceiveCallback requestCallBack) {

        post(params,requestCallBack,false);


    }

    public void get(RequestParams params,
                    final OnReceiveCallback requestCallBack) {

        x.http().get(params, new Callback.CommonCallback<String>() {

            @Override
            public void onSuccess(String result) {

                if (requestCallBack != null) {
                    requestCallBack.onSuccess(result);
                }


            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {


                if (requestCallBack != null) {

                    if (ex instanceof HttpException) {
                        HttpException httpEx = (HttpException) ex;
                        int responseCode = httpEx.getCode();
                        String responseMsg = httpEx.getMessage();
                        String errorResult = httpEx.getResult();
                        requestCallBack.onErro(responseCode + "");

                        return;
                    }
                    requestCallBack.onErro(ex.getMessage());
                }

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });


    }


    public interface OnReceiveCallback {


        void onSuccess(String result);

        void onErro(String msg);
    }


}
