package cn.mingzhi.android.artapp.widged;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;


import com.umeng.socialize.bean.SHARE_MEDIA;

import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.interfaces.ShareClickListener;
import cn.mingzhi.android.artapp.utills.Utils;


/**
 * Created by Administrator on 2015/8/11.
 */
public class ShareDialog extends Dialog implements View.OnClickListener {

    private View mView;
    private WindowManager.LayoutParams lp;
    private ShareClickListener mShareListener;
    private LinearLayout ll_wechat_friend, ll_wechat_circle, ll_yixin_friend, ll_yixin_circle, ll_qq_friend, ll_qq_space, ll_sina, ll_copy;
    private Button btn_cancel_share;

    public ShareDialog(Activity context) {
        super(context, R.style.dialog);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.share_dialog_layout, null);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setContentView(mView);
        lp = getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = Utils.getDisplaySize(context).widthPixels;
        getWindow().setAttributes(lp);
        initView();
    }

    private void initView() {
//        ll_wechat_friend = (LinearLayout) mView.findViewById(R.id.ll_wechat_friend);
//        ll_wechat_circle = (LinearLayout) mView.findViewById(R.id.ll_wechat_circle);
//        ll_yixin_friend = (LinearLayout) mView.findViewById(R.id.ll_yixin_friend);
//        ll_yixin_circle = (LinearLayout) mView.findViewById(R.id.ll_yixin_circle);
//        ll_qq_friend = (LinearLayout) mView.findViewById(R.id.ll_qq_friend);
//        ll_qq_space = (LinearLayout) mView.findViewById(R.id.ll_qq_space);
//        ll_sina = (LinearLayout) mView.findViewById(R.id.ll_sina);
//        ll_copy = (LinearLayout) mView.findViewById(R.id.ll_copy);
//        btn_cancel_share = (Button) findViewById(R.id.btn_cancel_share);
//        btn_cancel_share.setOnClickListener(this);
//        ll_wechat_friend.setOnClickListener(this);
//        ll_wechat_circle.setOnClickListener(this);
//        ll_yixin_friend.setOnClickListener(this);
//        ll_yixin_circle.setOnClickListener(this);
//        ll_qq_friend.setOnClickListener(this);
//        ll_qq_space.setOnClickListener(this);
//        ll_sina.setOnClickListener(this);
//        ll_copy.setOnClickListener(this);
    }

    public void setOnShareListener(ShareClickListener clickListener) {
        this.mShareListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.ll_wechat_friend:
//                mShareListener.onClick(SHARE_MEDIA.WEIXIN);
//                break;
//            case R.id.ll_wechat_circle:
//                mShareListener.onClick(SHARE_MEDIA.WEIXIN_CIRCLE);
//                break;
//            case R.id.ll_yixin_friend:
//                mShareListener.onClick(SHARE_MEDIA.YIXIN);
//                break;
//            case R.id.ll_yixin_circle:
//                mShareListener.onClick(SHARE_MEDIA.YIXIN_CIRCLE);
//                break;
//            case R.id.ll_qq_friend:
//                mShareListener.onClick(SHARE_MEDIA.QQ);
//                break;
//            case R.id.ll_qq_space:
//                mShareListener.onClick(SHARE_MEDIA.QZONE);
//                break;
//            case R.id.ll_sina:
//                mShareListener.onClick(SHARE_MEDIA.SINA);
//                break;
//            case R.id.ll_copy:
//                mShareListener.onCopy();
//                break;
//            case R.id.btn_cancel_share:
//                dismiss();
//                break;
        }
    }
}
