package cn.mingzhi.android.artapp.listener;

import android.view.View;

/**
 * Created by zhangbinglei on 15/4/16.
 */
public interface MyMutiOnitemClickListener {

    void onItemClick(View v, Object object);
}
