package cn.mingzhi.android.artapp.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.umeng.socialize.PlatformConfig;
import org.xutils.x;
import java.io.File;
import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.constants.Constants;


/**
 * Created by lugang on 2016/1/20.
 */
public class NewsApplication extends Application {

    public static NewsApplication mInstantce;

    private static Toast toast;
    public SharedPreferences sharedPreferences;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstantce = this;

        initThridLogin();
        initImageLoader(this);


        x.Ext.init(this);

        x.Ext.setDebug(true);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

    }

    public int[] getWidhtAndHeight() {


        int[] ints = new int[2];

        WindowManager wm = (WindowManager) this
                .getSystemService(Context.WINDOW_SERVICE);

        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();

        ints[0] = width;
        ints[1] = height;


        return ints;


    }


    /**
     * 获取Application
     */
    public static NewsApplication getApp() {
        return mInstantce;
    }


    public void clearAppCache() {
    }


    //初始化第三方登录
    private void initThridLogin() {
        //微信 appid appsecret
        PlatformConfig.setWeixin("wx967daebe835fbeac", "5bb696d9ccd75a38c8a0bfe0675559b3");
        //新浪微博 appkey appsecret
        PlatformConfig.setSinaWeibo("3516727714", "74f443529668afd60ff922e67c5481fb");
        // QQ和Qzone appid appkey
        PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");

        PlatformConfig.setYixin("yxc0614e80c9304c11b0391514d09f13bf");
    }

    //    diskCache(
//            new LruDiscCache(file, new HashCodeFileNameGenerator(),
//    50 * 1024 * 1024, 300))
    private void initImageLoader(Context applicationContext) {

        FadeInBitmapDisplayer fadeInBitmapDisplayer = new FadeInBitmapDisplayer(800, true, true, false);
        RoundedBitmapDisplayer roundedBitmapDisplayer = new RoundedBitmapDisplayer(3, 2);


        File file = Constants.getImageCacheFilePath();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
//                .showImageForEmptyUri(R.drawable.pic_default_new)
//                .showImageOnFail(R.drawable.pic_default_new)

                .cacheInMemory(true)
                .displayer(fadeInBitmapDisplayer)
                .cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                applicationContext).denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .diskCache(new UnlimitedDiskCache(file))
                .defaultDisplayImageOptions(options).build();

        ImageLoader.getInstance().init(config);

    }

    public static NewsApplication getInstance() {
        return mInstantce;
    }


    public Toast getToast() {

        if (toast == null) {
            toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        }

        return toast;
    }

    public void showToast(String msg) {

        Toast toast = getToast();
        toast.setText(msg);
        toast.show();
    }


}
