package cn.mingzhi.android.artapp.config;

/**
 * Created by zhangbinglei on 15/5/28.
 */
public class Config {
    public static boolean DEBUG = true; //是否输入日志1
    public static boolean ONLINE = true;//是否是线上
    public static boolean ISTEST = false;//是不是测试环境
    public static String VERSION = "300";//接口版本
    public static int databaseVersion = 5;
    /**
     * 测试环境针对所有人推送的tag前缀`````````````````````
     */
    public static final String DEBUG_PUSH_FORHEAD = "test_";
}

