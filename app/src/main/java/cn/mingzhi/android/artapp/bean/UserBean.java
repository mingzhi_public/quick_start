package cn.mingzhi.android.artapp.bean;


import java.io.Serializable;

/**
 * Created by zhangbinglei on 16/4/27.
 */


public class UserBean implements Serializable {

    //    {
//        "errCode": 0,
//            "errMsg": "成功",
//            "responseData": {
//        "username": "test",
//                "userId": "1",
//                "mobile": "ddd",
//                "messageFlag": 2
//    }
//    }
//
    private String userId;

    private String mobile;
    private String username;
    private String avatar;


    @Override
    public String toString() {
        return "UserBean{" +
                "userId='" + userId + '\'' +
                ", mobile='" + mobile + '\'' +
                ", username='" + username + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
