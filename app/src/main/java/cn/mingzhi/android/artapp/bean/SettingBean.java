package cn.mingzhi.android.artapp.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangbinglei on 16/7/5.
 */
public class SettingBean implements Serializable {
    private List<String> searchRecord; //搜索记录
    private String night="0";//0,1分别代表白天 夜间
    private String push ="0";//接受推送 0,1分别代表开关

}
