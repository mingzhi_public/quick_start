package cn.mingzhi.android.artapp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.google.gson.reflect.TypeToken;

import java.util.List;

import cn.finalteam.loadingviewfinal.OnDefaultRefreshListener;
import cn.finalteam.loadingviewfinal.OnLoadMoreListener;
import cn.finalteam.loadingviewfinal.PtrClassicFrameLayout;
import cn.finalteam.loadingviewfinal.PtrFrameLayout;
import cn.finalteam.loadingviewfinal.RecyclerViewFinal;
import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.adapter.FragMessageAdapter;
import cn.mingzhi.android.artapp.base.BaseFragment;
import cn.mingzhi.android.artapp.bean.FragMessageBean;
import cn.mingzhi.android.artapp.constants.Urls;
import cn.mingzhi.android.artapp.listener.MyMutiOnitemClickListener;
import cn.mingzhi.android.artapp.network.GetParamsUtill;
import cn.mingzhi.android.artapp.network.NetWorkUtill;
import cn.mingzhi.android.artapp.widged.MyEmptyView;

/**
 * Created by zhangbinglei on 16/8/26.
 */
public class DataFragment extends BaseFragment {




    MyEmptyView emptyView;

    RecyclerViewFinal lv;
    PtrClassicFrameLayout refreshLayout;
    List<FragMessageBean> tempList;

    FragMessageAdapter adapter;

    boolean isNew=true;

    int page=1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter=new FragMessageAdapter(getActivity(), new MyMutiOnitemClickListener() {
            @Override
            public void onItemClick(View v, Object object) {

            }
        });
    }

    @Override
    protected void lazyLoad() {


    }

    @Override
    public View initView(LayoutInflater inflater) {
        return mInitView(inflater);
    }

    private View mInitView(LayoutInflater inflater) {

        View v = inflater.inflate(R.layout.frag_art_circle, null, false);

        emptyView=(MyEmptyView)v.findViewById(R.id.empty_view);
        emptyView.setOnEmptyListener(new MyEmptyView.OnEmptyListener() {
            @Override
            public void onErroClick() {
                loadData();
            }
        });

        lv=(RecyclerViewFinal) v.findViewById(R.id.frag_art_circle_lv);

        refreshLayout=(PtrClassicFrameLayout) v.findViewById(R.id.frag_art_circle_refresh_layout);

        refreshLayout.setOnRefreshListener(new OnDefaultRefreshListener() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                isNew=true;
                page=1;
                loadData();

            }
        });

         lv.setOnLoadMoreListener(new OnLoadMoreListener() {
             @Override
             public void loadMore() {
                 isNew=false;
                 page++;
                 loadData();

             }
         });

        return v;
    }


    @Override
    public void loadData() {

        GetParamsUtill paramsUtill=new GetParamsUtill(Urls.MESSAGE_URL);

        netWorkUtill.netList(paramsUtill.getParams(),1001,this,new TypeToken<List<FragMessageBean>>() {
        });

    }

    @Override
    public void onReceive(int action, String code, String msg, Object data) {
        super.onReceive(action, code, msg, data);
        emptyView.success();
        if (action==1001){
            result(code,msg,data);
        }

    }

    private void result(String code, String msg, Object data) {

        if (code.equals(NetWorkUtill.OK)){

            List returnList=(List)data;

            if (isNew){

                tempList=returnList;
            }else {
                tempList.addAll(returnList);
            }

            adapter.setList(tempList);

        }

    }

    @Override
    public void onFail(int action, Object data) {
        emptyView.empty("没有消息，点击刷新");
        super.onFail(action, data);
    }
}
