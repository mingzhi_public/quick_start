package cn.mingzhi.android.artapp.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangbinglei on 16/7/26.
 */
public class ErroMessage {

    private static Map<Integer ,String>  map=new HashMap<>();

//    MEDIA_ERROR_UNKNOWN 	-1 	未知错误
//    ERROR_CODE_INVALID_URI 	-2 	无效的 URL
//    ERROR_CODE_IO_ERROR 	-5 	网络异常
//    ERROR_CODE_STREAM_DISCONNECTED 	-11 	与服务器连接断开
//    ERROR_CODE_EMPTY_PLAYLIST 	-541478725 	空的播放列表
//    ERROR_CODE_404_NOT_FOUND 	-875574520 	播放资源不存在
//    ERROR_CODE_CONNECTION_REFUSED 	-111 	服务器拒绝连接
//    ERROR_CODE_CONNECTION_TIMEOUT 	-110 	连接超时
//    ERROR_CODE_UNAUTHORIZED 	-825242872 	未授权，播放一个禁播的流
//    ERROR_CODE_PREPARE_TIMEOUT 	-2001 	播放器准备超时
//    ERROR_CODE_READ_FRAME_TIMEOUT 	-2002 	读取数据超时
    static {

        map.put(-1,"位置错误");
        map.put(-2,"无效的 URL");
        map.put(-5,"网络异常");
        map.put(-11,"与服务器连接断开");
        map.put(-541478725,"空的播放列表");
        map.put(-875574520,"播放资源不存在");
        map.put(-111,"服务器拒绝连接");
        map.put(-110,"连接超时");
        map.put(-825242872,"播放一个禁播的流");
        map.put(-2001,"播放器准备超时");
        map.put(-2002,"读取数据超时");




    }


    public static String  getErroMessage(int erroCode){

        return  map.get(erroCode);


    }
}
