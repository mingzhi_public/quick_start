package cn.mingzhi.android.artapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.base.BaseActivity;
import cn.mingzhi.android.artapp.bean.UserBean;
import cn.mingzhi.android.artapp.constants.Urls;
import cn.mingzhi.android.artapp.network.GetParamsUtill;
import cn.mingzhi.android.artapp.network.NetWorkUtill;
import cn.mingzhi.android.artapp.utills.StringUtill;


/**
 * Created by zhangbinglei on 16/6/20.
 */
public class LoginAct extends BaseActivity {


    private View loginLayout;

    private EditText accountEdit, passEidt;
    private int LOGIN_ACTION = 1001;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        initView();

//        goMainView();

    }


    @Override
    public void initView() {
        super.initView();

        loginLayout = findViewById(R.id.act_login_login_layout);
        accountEdit = (EditText) findViewById(R.id.act_login_account);
        passEidt = (EditText) findViewById(R.id.act_login_pass);
        findViewById(R.id.act_login_login).setOnClickListener(this);

        if (checkLogin() == null) {

            loginLayout.animate().alpha(1f).setDuration(1500).setStartDelay(1200).start();
        } else {
            new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    goMainView();
                }
            }.sendEmptyMessageDelayed(0, 3000);


        }


    }

    private void adjust() {


        String account = accountEdit.getText().toString();
        String pass = passEidt.getText().toString();

        if (StringUtill.isEmpty(account)) {

            showToast("用户名不能为空");

            return;
        }


        if (StringUtill.isEmpty(pass)) {

            showToast("密码不能为空");

            return;
        }


        if (pass.length() < 6) {

            showToast("密码不能少于6位数");
            return;
        }
        login(account, pass);


    }

    @Override
    public void onReceive(int action, String code, String msg, Object data) {
        super.onReceive(action, code, msg, data);


       stopProgressDialog();
        if (action == LOGIN_ACTION) {
            loginResult(code, msg, data);
        }
    }

    private void loginResult(String code, String msg, Object data) {


        if (code.equals(NetWorkUtill.OK)) {
            UserBean userBean = (UserBean) data;
            userBean.setUsername(accountEdit.getText().toString());
            saveUserBean(userBean);

            showToast("登录成功");

            goMainView();
        } else {
            showToast(msg);
        }

    }

    private void goMainView() {

        Intent intent = new Intent(this, MainAct.class);

        startActivity(intent);

        finish();

    }

    public void login(String account, String pass) {
        GetParamsUtill paramUtill = new GetParamsUtill(Urls.LOGIN_URL);
        paramUtill.add("account", account);
        paramUtill.add("pass", pass, true);
//        NetWorkUtill.netPost(Constants.LOGIN_URL, paramUtill.getParams(), LOGIN_ACTION, this);
        new NetWorkUtill().netObject(paramUtill.getParams(true), LOGIN_ACTION, this,UserBean.class);
        startProgressDialog();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.act_login_login:
                adjust();
                break;
        }

    }


}
