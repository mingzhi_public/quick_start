package cn.mingzhi.android.artapp.listener;

public interface MyJSReceiveDataListener {

	void onReceive(int action, String methodName, String code, String msg, Object data);


	void onFail(int action, String methodName, Object data);
}
