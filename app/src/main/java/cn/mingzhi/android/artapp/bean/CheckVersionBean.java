package cn.mingzhi.android.artapp.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/7/24.
 */
public class CheckVersionBean implements Serializable {
    private String message;                             // 消息
    private String is_needs_update;                     // 1-需要更新 2-强制更新 0-无需更新
    private String down_load_url;                       // apk下载地址
    private String down_patch_url;                      // patch下载地址
    private String is_patch;                            // 0-不启用增量跟新 1-启用增量更新
    private String version_name;
    // 版本名称
    private String version_code;                        // 版本号

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_needs_update() {
        return is_needs_update;
    }

    public void setIs_needs_update(String is_needs_update) {
        this.is_needs_update = is_needs_update;
    }

    public String getDown_load_url() {
        return down_load_url;
    }

    public void setDown_load_url(String down_load_url) {
        this.down_load_url = down_load_url;
    }

    public String getDown_patch_url() {
        return down_patch_url;
    }

    public void setDown_patch_url(String down_patch_url) {
        this.down_patch_url = down_patch_url;
    }

    public String getIs_patch() {
        return is_patch;
    }

    public void setIs_patch(String is_patch) {
        this.is_patch = is_patch;
    }

    public String getVersion_name() {
        return version_name;
    }

    public void setVersion_name(String version_name) {
        this.version_name = version_name;
    }

    public String getVersion_code() {
        return version_code;
    }

    public void setVersion_code(String version_code) {
        this.version_code = version_code;
    }
}
