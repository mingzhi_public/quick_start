package cn.mingzhi.android.artapp.network;



import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.mingzhi.android.artapp.constants.Constants;
import cn.mingzhi.android.artapp.utills.LogUtill;
import cn.mingzhi.android.artapp.utills.MD5Util;
import cn.mingzhi.android.artapp.utills.StringUtill;


public class GetParamsUtill {

    private String url = "";

    private String LOG_TAG = "GetParamsUtill";

    public  GetParamsUtill(){

    }
    public GetParamsUtill(String url) {
        this.url = url;
    }
    public void addUrl(String url){
        this.url = url;
    }

    public static String GLOBAL_PARAMS_KEY = "authcode";//


    private String appendParams;

    Map<String, String> paramMap = new HashMap<String, String>();

    public void add(String name, String value) {


        this.paramMap.put(name, value);

    }

    public void add(String name, String value, boolean isMd5) {
        if (isMd5) {
            value = MD5Util.getMD5Str(value);
        }
        this.paramMap.put(name, value);
    }


    public RequestParams getParams() {

        return getParams(true);
    }

    /**
     * 对参数排序后添加
     *
     * @param
     */
    public RequestParams getParams(boolean isNeesBase) {

        RequestParams requestParams = null;
//        paramMap.put(GLOBAL_PARAMS_KEY, StringUtill.getBase64Str(url));
        if (isNeesBase) {
            requestParams = new RequestParams(Constants.BASE_URL + url);
        } else {
            requestParams = new RequestParams(url);
        }
        

        requestParams.setMethod(HttpMethod.POST);

        List<String> paramKeyList = new ArrayList<String>();


        Set<String> keySet = paramMap.keySet();
        for (String key : keySet) {

            paramKeyList.add(key);


        }
        List<String> newKeyList = StringUtill.sortStr(paramKeyList);

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");

        for (String key : newKeyList) {


            requestParams.addQueryStringParameter(key, paramMap.get(key));

            stringBuffer.append(key)
                    .append("=").append(paramMap.get(key)).append("&");


        }

//        requestParams.addQueryStringParameter(GLOBAL_PARAMS_KEY, StringUtill.getBase64Str(url));

        this.appendParams = stringBuffer.substring(0, stringBuffer.length() - 1);


        LogUtill.print(LOG_TAG, "网络请求url", getApandParams(isNeesBase));
        return requestParams;
    }

    private String getApandParams(boolean isNeedBase) {

        if (url == null) {

            url = "";
        }

        if (isNeedBase){
            return Constants.BASE_URL + url + this.appendParams;
        }else {
            return  url + this.appendParams;
        }




    }

}
