package cn.mingzhi.android.artapp.interfaces;

import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * Created by Administrator on 2015/8/11.
 */
public interface ShareClickListener {

     void onClick(SHARE_MEDIA platform);
     void onCopy();

}
