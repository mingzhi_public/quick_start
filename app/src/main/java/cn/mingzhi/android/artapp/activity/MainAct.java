package cn.mingzhi.android.artapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;


import java.util.ArrayList;

import cn.mingzhi.android.artapp.R;
import cn.mingzhi.android.artapp.base.BaseActivity;
import cn.mingzhi.android.artapp.bean.TabEntity;
import cn.mingzhi.android.artapp.fragment.ArtCircleFragment;
import cn.mingzhi.android.artapp.fragment.DataFragment;
import cn.mingzhi.android.artapp.fragment.MessageFragment;
import cn.mingzhi.android.tablib.CommonTabLayout;
import cn.mingzhi.android.tablib.listener.CustomTabEntity;
import cn.mingzhi.android.tablib.listener.OnTabSelectListener;

/**
 * Created by zhangbinglei on 16/8/26.
 */
public class MainAct extends BaseActivity{
    private int[] mIconUnselectIds = {
            R.drawable.ic_main_message, R.drawable.ic_main_art,
            R.drawable.ic_main_data, R.drawable.ic_main_person};
    private int[] mIconSelectIds =  {
            R.drawable.ic_main_message, R.drawable.ic_main_art,
            R.drawable.ic_main_data, R.drawable.ic_main_person};

    private CommonTabLayout tagLayout;
    private String[] mTitles = {"消息", "艺术圈", "数据", "个人中心"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        initArgs();
        initView();
    }

    @Override
    public void initArgs() {
        super.initArgs();

        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }


    }

    @Override
    public void initView() {
        super.initView();

        tagLayout =(CommonTabLayout) findViewById(R.id.act_main_tablayout);
        tagLayout.setTabData(mTabEntities);

        tagLayout.setCurrentTab(0
        );
        getSupportFragmentManager().beginTransaction().add(R.id.act_main_frag_container,new MessageFragment(),mTitles[0]);
        getSupportFragmentManager().beginTransaction().add(R.id.act_main_frag_container,new ArtCircleFragment(),mTitles[1]);
        getSupportFragmentManager().beginTransaction().add(R.id.act_main_frag_container,new DataFragment(),mTitles[2]);
        getSupportFragmentManager().beginTransaction().add(R.id.act_main_frag_container,new MessageFragment(),mTitles[3]);

        setCurrent(0);

        tagLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {

                setCurrent(position);

            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }
    public void setCurrent(int index){



        for (int i=0;i<mTitles.length;i++){

            if (index==i){

                getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager().findFragmentByTag(mTitles[i]));
            }else {
                getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag(mTitles[i]));
            }
        }
    }
}
