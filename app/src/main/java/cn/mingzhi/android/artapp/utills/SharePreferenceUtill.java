package cn.mingzhi.android.artapp.utills;

import android.content.Context;



import java.io.IOException;

import cn.mingzhi.android.artapp.bean.SettingBean;
import cn.mingzhi.android.artapp.bean.UserBean;

public class SharePreferenceUtill extends BaseSharePreferenceUtill {
    public static String USER_BEAN_KEY = "userBean";
    private static String SETTING_BEAN_KEY = "settingBean";

    public SharePreferenceUtill(Context context) {
        super(context);
    }

    private static SharePreferenceUtill sharePreferenceUtill;

    public static SharePreferenceUtill getInstance(Context context) {
        if (sharePreferenceUtill == null) {
            sharePreferenceUtill = new SharePreferenceUtill(context);
        }
        return sharePreferenceUtill;
    }

    public UserBean getUserbean() {


        UserBean userBean = null;

        try {
            userBean = (UserBean) getObjectFromLocal(USER_BEAN_KEY);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (userBean == null) {


            return null;
        }

        if (StringUtill.isEmpty(userBean.getUserId() + "")) {

            return null;
        }
        return userBean;
    }

    public SettingBean getSetting() {


        SettingBean settingBean = null;

        try {
            settingBean = (SettingBean) getObjectFromLocal(SETTING_BEAN_KEY);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(settingBean==null){
            settingBean = new SettingBean();
        }
        return  settingBean;
    }




    public void clearKey(String keys) {

        clear(keys);

    }

    public void saveUserBean(UserBean userBean) {

        try {
            saveObjectToLocal(userBean, USER_BEAN_KEY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveSettingBean(SettingBean settingBean) {

        try {
            saveObjectToLocal(settingBean, SETTING_BEAN_KEY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
