package cn.mingzhi.android.artapp.utills;

import android.content.Context;
import android.content.SharedPreferences;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import cn.mingzhi.android.artapp.constants.Constants;

/**
 * Created by zhangbinglei on 16/4/27.
 */
public class BaseSharePreferenceUtill {

    private final String LOG_TAG = "SharePreferenceUtill";
    private static String CONFIG = "config";
    private static SharedPreferences sharedPreferences;



    public BaseSharePreferenceUtill(Context context) {

        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.CONFIG, 0);
        }


    }

    public void clear(String key){
        sharedPreferences.edit().remove(key).commit();
    }


    //存储
    public void saveStringData(String key, String value) {


        sharedPreferences.edit().putString(key, value).commit();
    }

    //存储boolean值
    public void saveBooleanData(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).commit();
    }

    //获取

    public String getStringData(String key, String defValue) {

        return sharedPreferences.getString(key, defValue);
    }

    //获取boolean值
    public boolean getBooleanData(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    //移除某一个key
    public void removeKey(String keyName) {

        sharedPreferences.edit().remove(keyName).commit();
    }


    /**
     * 存储对象到本地
     *
     * @param obj
     */
    public void saveObjectToLocal(Object obj, String key) throws IOException {
        saveObject(serialize(obj), key);
    }
    public Object getObjectFromLocal(String key) throws IOException, ClassNotFoundException {
        return deSerialization(getStringData(key, null));
    }

    public void saveObject(String strObject, String key) {
        saveStringData(key, strObject);
    }

    /**
     * 序列化对象
     *
     * @return
     * @throws IOException
     */
    private String serialize(Object obj) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                byteArrayOutputStream);
        objectOutputStream.writeObject(obj);
        String serStr = byteArrayOutputStream.toString("ISO-8859-1");
        serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
        objectOutputStream.close();
        byteArrayOutputStream.close();
        return serStr;
    }



    /**
     * 反序列化对象
     *
     * @param str
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private Object deSerialization(String str) throws IOException, ClassNotFoundException {
        if (StringUtill.isEmpty(str)){

            return null;
        }

        String redStr = java.net.URLDecoder.decode(str, "UTF-8");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                redStr.getBytes("ISO-8859-1"));
        ObjectInputStream objectInputStream = new ObjectInputStream(
                byteArrayInputStream);
        Object obj = (Object) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        return obj;
    }

}
