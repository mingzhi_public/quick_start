package cn.mingzhi.android.artapp.constants;

import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;



import java.io.File;
import java.util.UUID;

import cn.mingzhi.android.artapp.config.Config;

public class Constants {

    //访问服务端的地址
//    http://www.inglian.cn/  http://ytzhixuan20.52yingzheng.com:80/
    public static boolean isNightMode = false;
    public static String BASE_URL = "http://192.168.92.245:8080/app/getTuData/";
    public static String TEST_URL = "http://192.168.92.245:8080/";
    public static String BASE_ASSETS_URL = "file:///android_asset/";
    ;
//    public static String BASE_URL = "http://www.inglian.cn/";

    public static String CONFIG = "com.mingzhi.android.interfacetestapp.config";
    public static int MAIN_PAGE_TAB = 1;


    private static boolean LOG_TAG = true;


    public static final String WXAPPID = "wx91c3c71e376ebde1";

    public static final String WXSECRET = "4cb12d3550d11a65cc3e67ee2a6bf607";

    public static final String _NAVIGATE = "com.jsonhu.people_news.activity.MainActivity";
    public static final String _SKINSPRITE = "com.jsonhu.people_news.fragment.MinePager";

    public static final int VIEW_TYPE_NORMAL = 1001;//普通
    public static final int VIEW_TYPE_VEDIO = 1002;//视频
    public static final int VIEW_TYPE_AUDIO = 1003;//音频
    public static final int VIEW_TYPE_AVATAR = 1004;//多图
    public static final int VIEW_TYPE_RECOMMEND = 1005;//推荐
    public static final int VIEW_TYPE_LIVE = 1006;//直播
    public static final int VIEW_SPECIAL_TYPE_TITLE = 1007;//类型
    public static final int ACTION_COMMENT = 100001;
    public static final int ACTION_REPLY = 100002;
    public static final String ACTION_COLLECTBROAD = "ACTION_COLLECTBROAD";
    public static final String ACTION_COMMENTBROAD = "ACTION_COMMENTBROAD";

    static {
        if (Config.ONLINE) {
            if (Config.ISTEST) {

                BASE_URL = "http://cms.rmrb.bliaoliao.cn/Api/";
//                BASE_URL = "http://10.2.245.233:8080/";

//                BASE_URL = "http://rmrb_cms.52yingzheng.com/Api/";

            } else {
                BASE_URL = "http://rmrbcms.52yingzheng.com/Api/";
            }

        } else {
            BASE_URL = "http://apiTest.52yingzheng.com/";
        }
        LOG_TAG = Config.DEBUG;

    }

    public static void log_i(String className, String tag, String text) {
        if (LOG_TAG) {
            Log.i(className, tag + "----->>" + text);
        }
    }

    public static void print(String className, String tag) {
        if (LOG_TAG) {
            print(className, tag, "");
        }
    }

    public static void print(String className, String tag, String text) {
        if (LOG_TAG) {
            Log.i(className, tag + "------>>" + text);
        }
    }

    public static File getImageCacheFilePath() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File f = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "//com.lingsi.rmrb//imageCache");
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            return f;
        } else {
            return null;
        }
    }

    /**
     * 获取uuid
     *
     * @param context
     * @return
     */
    public static String getUUID(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, tmPhone, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String uniqueId = deviceUuid.toString();
        return uniqueId;
    }
    
//    public static boolean sCONTROLLER_AUDIO_HEAD_SHOW = true;
//    public static boolean sMEDIA_PLAYING = false;

    public static String sDEVICE_IMEI = "";

    /**
     * 观察者模式-key-时间模式切换
     */
    public static final int OBSERVER_KEY_TIEMMODE = 0x370;

    /**
     * 时间模式-白天
     */
    public static final int TIME_MODE_DAY = 0x31;

    /**
     * 时间模式-夜间
     */
    public static final int TIME_MODE_NIGHT = 0x32;

    /**
     * 观察者模式-key-图片模式切换
     */
    public static final int OBSERVER_KEY_IMAGEMODE = 0x381;

    /**
     * 图片模式-有图
     */
    public static final int IMAGE_MODE_VISIBLE = 0x55;

    /**
     * 图片模式-无图
     */
    public static final int IMAGE_MODE_GONE = 0x56;


}
