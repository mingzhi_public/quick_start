package cn.mingzhi.android.artapp.network;


import android.util.Log;


import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import cn.mingzhi.android.artapp.bean.UserBean;
import cn.mingzhi.android.artapp.constants.Constants;
import cn.mingzhi.android.artapp.listener.MyReceiveDataListener;
import cn.mingzhi.android.artapp.utills.GsonUtill;


/**
 * Created by zhangbinglei on 15/5/15.
 */
public class NetWorkUtill extends BaseNetworkUtill {

    private static String LOG_TAG = "NetWorkUtill2_0";

    /**
     * 获得一个对象
     *
     * @param params
     * @param action
     * @param myReceiveDataListener
     */
    public void netObject(RequestParams params, final int action, final MyReceiveDataListener myReceiveDataListener, final Class cls) {


        post(params, new OnReceiveCallback() {
            @Override
            public void onSuccess(String result) {
                String code = "-1";

                String msg = "返回结果错误";

                Constants.print(LOG_TAG, "登录返回结果", result);


                Object object = null;

                try {
                    JSONObject jso = new JSONObject(result.toString());

                    code = jso.getString("errCode");

                    msg = jso.getString("errMsg");


                    object = GsonUtill.getObejctFromJSON(jso.getJSONObject("responseData").toString(), cls);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (myReceiveDataListener != null) {
                    myReceiveDataListener.onReceive(action, code, msg, object);
                }

            }

            @Override
            public void onErro(String msg) {
                if (myReceiveDataListener != null) {

                    myReceiveDataListener.onFail(action, msg);
                }

            }
        });


    }

    /**
     * 获得一个列表
     *
     * @param params
     * @param action
     * @param myReceiveDataListener
     */
    public void netList(RequestParams params, final int action, final MyReceiveDataListener myReceiveDataListener, final TypeToken typeToken) {


        post(params, new OnReceiveCallback() {
            @Override
            public void onSuccess(String result) {
                String code = "-1";

                String msg = "返回结果错误";

                Constants.print(LOG_TAG, "登录返回结果", result);


                Object object = null;

                try {
                    JSONObject jso = new JSONObject(result.toString());

                    code = jso.getString("errCode");

                    msg = jso.getString("errMsg");


                    object = GsonUtill.getListObjectFromJSON(jso.getJSONArray("data").toString(), typeToken);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (myReceiveDataListener != null) {
                    myReceiveDataListener.onReceive(action, code, msg, object);
                }

            }

            @Override
            public void onErro(String msg) {
                if (myReceiveDataListener != null) {

                    myReceiveDataListener.onFail(action, msg);
                }

            }
        });


    }


}
