package cn.mingzhi.android.artapp.widged;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by zhangbinglei on 16/8/26.
 */
public class ExpandableLinnerLayout extends LinearLayout {

    private int minHeight;
    private int height;

    public ExpandableLinnerLayout(Context context) {
        super(context);
    }

    public ExpandableLinnerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableLinnerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);


        if (height==0){
            height=getMeasuredHeight();//记录当前的高度
        }


    }

    public void tog() {

        ValueAnimator valueAnimator = null;
        if (getHeight() > minHeight) {
            valueAnimator = ValueAnimator.ofInt(height, minHeight);


        } else {

            valueAnimator = ValueAnimator.ofInt(minHeight, height);
        }


        valueAnimator.setDuration(500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                ViewGroup.LayoutParams params = getLayoutParams();
                params.height = (int) animation.getAnimatedValue();

                setLayoutParams(params);
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        valueAnimator.start();
    }
}
